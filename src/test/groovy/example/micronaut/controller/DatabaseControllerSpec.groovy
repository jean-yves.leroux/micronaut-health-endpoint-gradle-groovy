package example.micronaut.controller

import io.micronaut.http.HttpRequest
import io.micronaut.http.client.HttpClient
import io.micronaut.http.client.annotation.Client
import io.micronaut.test.extensions.spock.annotation.MicronautTest
import jakarta.inject.Inject
import spock.lang.Specification

@MicronautTest
class DatabaseControllerSpec extends Specification{

    @Inject
    @Client("/")
    HttpClient client

    void "get endpoint return one value"() {
        when:
        def resp = client.toBlocking().retrieve(HttpRequest.GET("/private/open/database"), List)

        then:
        resp == ['3fc4b034-a3ca-42c9-bca8-f684018f4e10']
    }
}
