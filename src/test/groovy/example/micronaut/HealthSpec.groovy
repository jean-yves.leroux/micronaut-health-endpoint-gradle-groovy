package example.micronaut

import example.micronaut.client.InocsCoreClient
import io.micronaut.context.annotation.Property
import io.micronaut.context.annotation.Replaces
import io.micronaut.http.HttpRequest
import io.micronaut.http.HttpResponse
import io.micronaut.http.HttpStatus
import io.micronaut.http.client.HttpClient
import io.micronaut.http.client.annotation.Client
import io.micronaut.test.extensions.spock.annotation.MicronautTest
import jakarta.inject.Inject
import jakarta.inject.Singleton
import spock.lang.Specification

@Property(name = "endpoints.health.jdbc.enabled", value = "false")
@Property(name = "endpoints.health.disk-space.enabled", value = "false")
@MicronautTest // <1>
class HealthSpec extends Specification {

    @Inject
    @Client("/") // <2>
    HttpClient client

    void "health endpoint exposed"() {
        when:
        HttpStatus status = client.toBlocking().retrieve(HttpRequest.GET("/private/open/health"), HttpStatus) // <3>

        then:
        status == HttpStatus.OK
    }

    void "liveness health indicator"() {
        when:
        def health = client.toBlocking().retrieve(HttpRequest.GET("/private/open/health/liveness"), HashMap)

        then:
        health == [
                name   : 'micronautguide',
                details: [
                        micronautguide: [
                                name  : 'micronautguide',
                                status: 'UP'
                        ]
                ],
                status : 'UP'
        ]
    }

    void "readiness health indicator"() {
        when:
        def health = client.toBlocking().retrieve(HttpRequest.GET("/private/open/health/readiness"), HashMap)

        then:
        health == [
                name   : 'micronautguide',
                details: [
                        service     : [
                                name  : 'micronautguide',
                                status: 'UP'
                        ],
                        'inocs-core': [
                                name   : 'micronautguide',
                                status : 'UP',
                                details: [
                                        env  : 'local',
                                        key  : 'urlInocsCore',
                                        value: 'https://inocs-core.mock.com',
                                        url  : 'https://inocs-core.mock.com/private/open/health/liveness'
                                ]
                        ]
                ],
                status : 'UP'
        ]
    }
}

@Replaces(InocsCoreClient)
@Singleton
class InocsCoreClientMock implements InocsCoreClient {

    @Override
    HttpResponse healthLiveness() {
        HttpResponse.ok([status: 'UP'])
    }
}
