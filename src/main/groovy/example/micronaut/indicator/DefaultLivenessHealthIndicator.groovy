package example.micronaut.indicator

import io.micronaut.context.annotation.Requires
import io.micronaut.context.annotation.Value
import io.micronaut.management.endpoint.health.HealthEndpoint
import io.micronaut.management.health.indicator.AbstractHealthIndicator
import io.micronaut.management.health.indicator.annotation.Liveness
import jakarta.inject.Singleton

import static io.micronaut.health.HealthStatus.UP

@Liveness
@Singleton
@Requires(property = 'endpoints.health.enabled', value = 'true')
@Requires(bean = HealthEndpoint)
class DefaultLivenessHealthIndicator extends AbstractHealthIndicator {

    @Value('${micronaut.application.name}')
    String name

    @Override
    protected Map<String, Object> getHealthInformation() {
        healthStatus = UP
        [:]
    }
}
