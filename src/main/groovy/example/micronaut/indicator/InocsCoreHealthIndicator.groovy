package example.micronaut.indicator

import example.micronaut.client.InocsCoreClient
import io.micronaut.context.annotation.Requires
import io.micronaut.context.annotation.Value
import io.micronaut.management.endpoint.health.HealthEndpoint
import io.micronaut.management.health.indicator.annotation.Readiness
import jakarta.inject.Inject
import jakarta.inject.Singleton

@Readiness
@Singleton
@Requires(property = 'endpoints.health.enabled', value = 'true')
@Requires(bean = HealthEndpoint)
class InocsCoreHealthIndicator extends AbstractInocsHealthIndicator {

    String name = 'inocs-core'
    String key = 'urlInocsCore'
    @Value('${config.urlInocsCore}')
    def value
    @Value('${config.urlInocsCore}/private/open/health/readinessOrWhatYouWant')
    String url
    @Inject
    InocsCoreClient client
}
