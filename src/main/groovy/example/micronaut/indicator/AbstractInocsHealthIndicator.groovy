package example.micronaut.indicator

import example.micronaut.client.HealthClient
import io.micronaut.context.annotation.Value
import io.micronaut.management.health.indicator.AbstractHealthIndicator

import static io.micronaut.health.HealthStatus.DOWN
import static io.micronaut.health.HealthStatus.UNKNOWN
import static io.micronaut.health.HealthStatus.UP

abstract class AbstractInocsHealthIndicator extends AbstractHealthIndicator<Map<String, Object>> {

    @Value('${config.env}')
    String env

    abstract String getKey()

    abstract getValue()

    abstract String getUrl()

    abstract HealthClient getClient()

    @Override
    protected Map<String, Object> getHealthInformation() {
        def status = (client.healthLiveness().body() as Map).status
        healthStatus = status == 'UP' ? UP : status == 'DOWN' ? DOWN : UNKNOWN
        [
                env  : env,
                key  : key,
                value: value,
                url  : url as String
        ]
    }
}
