package example.micronaut.controller

import example.micronaut.service.DatabaseService
import io.micronaut.http.HttpResponse
import io.micronaut.http.annotation.Controller
import io.micronaut.http.annotation.Delete
import io.micronaut.http.annotation.Get
import io.micronaut.http.annotation.Put
import jakarta.inject.Inject

@Controller('/private/open/database')
class DatabaseController {

    @Inject
    DatabaseService service

    @Get
    def get() {
        HttpResponse.ok(service.get())
    }

    @Put
    def save() {
        service.save(UUID.randomUUID() as String)
        HttpResponse.ok(service.get())
    }

    @Delete
    def delete() {
        service.delete()
        HttpResponse.ok(service.get())
    }
}
