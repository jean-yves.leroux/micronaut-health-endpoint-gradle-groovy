package example.micronaut.client

import io.micronaut.http.client.annotation.Client
import io.micronaut.retry.annotation.Retryable

@Retryable(attempts = '${config.retry.attempts}', delay = '${config.retry.delay}', multiplier = '${config.retry.multiplier}')
@Client('http://${config.urlInocsCore}:49329')  // FIXME: doesn't work without protocal and port constants :(. Problem with InocsCoreHealthIndicator ? Using HttpClientConfiguration with/instead of ID ? See more at https://docs.micronaut.io/latest/api/io/micronaut/http/client/annotation/Client.html
interface InocsCoreClient extends HealthClient {
}
