package example.micronaut.client

import io.micronaut.http.HttpResponse
import io.micronaut.http.annotation.Get

interface HealthClient {

    @Get('/private/open/health/liveness')
    HttpResponse healthLiveness()

}
