package example.micronaut.service

import groovy.sql.Sql
import groovy.util.logging.Slf4j
import io.micronaut.data.jdbc.annotation.JdbcRepository
import io.micronaut.data.model.query.builder.sql.Dialect
import jakarta.inject.Singleton

import javax.sql.DataSource
import javax.transaction.Transactional

@Slf4j
@Singleton
@Transactional
@JdbcRepository(dialect = Dialect.POSTGRES)
class DatabaseService {

    DataSource dataSource

    DatabaseService(DataSource dataSource) {
        this.dataSource = dataSource
    }

    def get() {
        def md5values = []
        def sql = new Sql(dataSource)
        sql.query('''
            select md5value
            from stock
        ''', {
            while (it.next()) {
                md5values.add(it.getString('md5value'))
            }
        })
        md5values
    }

    def save(String md5) {
        def sql = new Sql(dataSource)
        sql.executeInsert("""
            insert 
            into stock
            values ($md5)
        """)
    }

    def delete() {
        def sql = new Sql(dataSource)
        sql.execute('''
            delete
            from stock
        ''')
    }
}
